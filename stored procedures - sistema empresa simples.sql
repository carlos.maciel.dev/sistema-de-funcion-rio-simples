-- criando as stored procedures
-- 1) Inserir dados para um novo funcionário.
DELIMITER $$
USE `apbd2019`$$
DROP PROCEDURE IF EXISTS sp_inserirFuncionario$$
CREATE PROCEDURE sp_inserirFuncionario (IN cpf varchar(11),IN nome varchar(50), IN diaNasc varchar(2), 
                                       IN mesNasc varchar(2), anoNasc varchar(4))
BEGIN
insert into Funcionario values(cpf, nome, diaNasc, mesNasc, anoNasc);
END $$
DELIMITER ;

CALL sp_inserirFuncionario('44444444','GIOVANNI MACIEL', '03', '11' , '2002');

select*from funcionario;

-- QUESTÃO 2

select*from Departamento d
inner join Projeto p on p.codDepto = d.codigo
where p.codigo = 1;

drop procedure lista_departamento;
DELIMITER $$
CREATE PROCEDURE lista_departamento(codigo INT)
BEGIN
select d.codigo, d.nome from Departamento d
inner join Projeto p on p.codDepto = d.codigo
where p.codigo = codigo;
END $$
DELIMITER ;

CALL lista_departamento(1);

-- 3) Atualizar o nome de um determinado funcionário conforme o seu CPF informado.

DELIMITER $$
USE `apbd2019`$$
DROP PROCEDURE IF EXISTS sp_atualizarNomeFuncionario$$
CREATE PROCEDURE sp_atualizarNomeFuncionario (IN cpfs varchar(11), IN nomes varchar(50))
BEGIN
update Funcionario
set nome = nomes
where cpf = cpfs;
END $$
DELIMITER ;
desc Empregado;
CALL sp_atualizarNomeFuncionario('11111111111', 'EDUARDO DINIZ');

CALL sp_atualizarNomeFuncionario('333333333', 'LUISA FONSECA');
CALL sp_atualizarNomeFuncionario('22222222222', 'Murilo Neves');
CALL sp_atualizarNomeFuncionario('44444444', 'Diego Arruda');


-- INICIO QUESTÃO 4
select e.nome, count(t.horas) from Trabalha_em t
inner join Funcionario e on t.cpf = e.cpf
inner join Projeto p on t.codProjeto = p.codigo
where ativo = 'S';

DROP PROCEDURE lista_funcionarios;
DELIMITER $$
CREATE PROCEDURE lista_funcionarios(status_projeto char)
BEGIN
select e.nome, t.horas from Trabalha_em t
inner join Funcionario e on t.cpf = e.cpf
inner join Projeto p on t.codProjeto = p.codigo
where ativo = 'S';
END $$
DELIMITER ;

CALL lista_funcionarios('S');

-- 5) Criar uma lista dos funcionários, contendo nome e dia, quando informado o mês.

DELIMITER $$
USE `apbd2019`$$
DROP PROCEDURE IF EXISTS sp_listarFuncionariosNomeDia$$
CREATE PROCEDURE sp_listarFuncionariosNomeDia (IN mes varchar(8))
BEGIN
select nome 'Nome', substring(dataNasc, 1,2)'Dia'
from Funcionario
where mes = substring(dataNasc, 3,2);  
END $$
DELIMITER ;

CALL sp_listarFuncionariosNomeDia('05');


-- QUESTÃO 6
-- alter table Trabalha_em drop foreign key Trabalha_em_ibfk_2;
select * from Projeto
where codigo = 1;

DROP PROCEDURE deleta_projeto;
DELIMITER $$
CREATE PROCEDURE deleta_projeto(codigo INT)
BEGIN
select * from Projeto
where codigo = codigo;
END $$
DELIMITER ;

CALL deleta_projeto(2);

-- 7) Retornar a data de nascimento de um determinado funcionário quando informado seu

DELIMITER $$
USE `apbd2019`$$
DROP PROCEDURE IF EXISTS sp_listarDataNascimentoFuncionario$$
CREATE PROCEDURE sp_listarDataNascimentoFuncionario (IN cpfs varchar(11))
BEGIN
select concat(substring(dataNasc, 1,2),('/'), substring(dataNasc, 3,2),('/'), substring(dataNasc, 5,4)) 'Data de Nascimento', nome 'Nome Funcionario'
from Funcionario
where cpf = cpfs;  
END $$
DELIMITER ;

CALL sp_listarDataNascimentoFuncionario('22222222222');


-- INICIO QUESTÃO 8
select*from Trabalha_em t
inner join Projeto p on p.codigo = t.codProjeto
where ativo = 'S';


DROP PROCEDURE lista_funcionarios_projetos
DELIMITER $$
CREATE PROCEDURE lista_funcionarios_projetos(estado char)
BEGIN
select*from Trabalha_em t
inner join Projeto p on p.codigo = t.codProjeto
where ativo = estado;
END $$
DELIMITER ;

CALL lista_funcionarios_projetos('S');


-- 9) Retornar o código e o nome completo do projeto ativo quando for informado apenas parte do nome deste projeto. 

DELIMITER $$
USE `apbd2019`$$
DROP PROCEDURE IF EXISTS sp_listarProjetoAtivo$$
CREATE PROCEDURE sp_listarProjetoAtivo (IN nomeproj varchar(11))
BEGIN
select codigo 'Código do Projeto', nome 'Nome do Projeto'
from Projeto 
where substring(nome, 1,3) = substring(nomeproj, 1,3)  && ativo = 'S';  
END $$
DELIMITER ;

CALL sp_listarProjetoAtivo('VEN');









