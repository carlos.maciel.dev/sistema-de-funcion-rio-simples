-- criação do banco de dados

CREATE TABLE Departamento(
codigo smallint not null auto_increment,
nome varchar(50) not null,
primary key (codigo),
unique (nome));

CREATE TABLE Funcionario(
cpf varchar (11) not null,
nome varchar(50) not null,
diaNasc varchar (2) not null,
mesNasc varchar (2) not null,
anoNasc varchar(4) not null,
primary key (cpf));


CREATE TABLE Projeto(
codigo smallint not null auto_increment,
nome varchar(200) not null,
ativo char not null,
codDepto smallint not null,
primary key (codigo),
foreign key (codDepto) References Departamento(codigo));


CREATE TABLE Trabalha_em(
cpf varchar (11) not null,
codProjeto smallint not null,
horas tinyint not null,
primary key (cpf, codProjeto),
foreign key (cpf) References Funcionario(cpf),
foreign key (codProjeto) References Projeto(codigo));
