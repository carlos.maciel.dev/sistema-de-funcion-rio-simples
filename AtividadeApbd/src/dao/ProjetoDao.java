/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Projeto;

/**
 *
 * @author CARLO
 */
public class ProjetoDao extends GenericDAO {
    public void salvar(Projeto projeto) throws SQLException {
        String insert = "INSERT INTO Projeto(codigo, nome, ativo,codDepto) VALUES(?,?,?,?)";
        save(insert, projeto.getCodigo(), projeto.getNome(), projeto.getAtivo(), projeto.getCodDepartamento());
    }

    public void alterar(Projeto projeto) throws SQLException {
        String update = "UPDATE Projeto " +
                "SET codigo = ?, nome = ?, ativo = ?, codDepto = ?" +
                "WHERE codigo = ?";
        update(update, projeto.getCodigo(), projeto.getNome(), projeto.getAtivo(), projeto.getCodDepartamento());
    }

    public void excluir(long codigo) throws SQLException {
        String delete = "DELETE FROM Projeto WHERE codigo = ?";
        delete(delete, codigo);
    }

    public List findProjetos() throws SQLException {
        List projetos = new ArrayList();

        String select = "SELECT * FROM projeto";

        PreparedStatement stmt = 
			getConnection().prepareStatement(select);
			
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            Projeto projeto = new Projeto();
            projeto.setCodigo(rs.getInt("codigo"));
            projeto.setNome(rs.getString("nome"));
            projeto.setAtivo(rs.getString("ativo"));
            

            
            projetos.add(projeto);
        }

        rs.close();
        stmt.close();
        getConnection().close();

        return projetos;
    }

    public Projeto findByName(String nome) throws SQLException {
        String select = "SELECT * FROM Projeto WHERE nome = ?";
        Projeto projeto = null;
        PreparedStatement stmt = 
			getConnection().prepareStatement(select);
			
        stmt.setString(1, nome);
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            projeto = new Projeto();
            projeto.setCodigo(rs.getInt("codigo"));
            projeto.setNome(rs.getString("nome"));
            projeto.setAtivo(rs.getString("ativo"));
        }

        rs.close();
        stmt.close();
        getConnection().close();

        return projeto;
    }
}
