/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author CARLO
 */


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Funcionario;

public class FuncionarioDao extends GenericDAO {

    public void salvar(Funcionario funcionario) throws SQLException {
        String insert = "INSERT INTO Funcionario(cpf, nome, diaNasc, mesNasc, anoNasc) VALUES(?,?,?,?,?)";
        save(insert, funcionario.getCpf(), funcionario.getNome(), funcionario.getDiaNasc(), funcionario.getMesNasc(), funcionario.getAnoNasc());
    }

    public void alterar(Funcionario funcionario) throws SQLException {
        String update = "UPDATE Funcionario " +
                "SET cpf = ?, nome = ?, diaNasc = ?, mesNasc = ?, anoNasc = ? " +
                "WHERE cpf = ?";
        update(update, funcionario.getCpf(), funcionario.getNome(), funcionario.getDiaNasc(), funcionario.getMesNasc(), funcionario.getAnoNasc());
    }

    public void excluir(long cpf) throws SQLException {
        String delete = "DELETE FROM Funcionario WHERE cpf = ?";
        delete(delete, cpf);
    }

    public List findFuncionarios() throws SQLException {
        List funcionarios = new ArrayList();

        String select = "SELECT * FROM Funcionario";

        PreparedStatement stmt = 
			getConnection().prepareStatement(select);
			
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            Funcionario funcionario = new Funcionario();
            funcionario.setCpf(rs.getInt("cpf"));
            funcionario.setNome(rs.getString("nome"));
            funcionario.setDiaNasc(rs.getString("diaNasc"));
            funcionario.setMesNasc(rs.getString("mesNasc"));
            funcionario.setAnoNasc(rs.getString("anoNasc"));
            funcionarios.add(funcionario);
        }

        rs.close();
        stmt.close();
        getConnection().close();

        return funcionarios;
    }

    public Funcionario findByName(String nome) throws SQLException {
        String select = "SELECT * FROM Funcionario WHERE nome = ?";
        Funcionario funcionario = null;
        PreparedStatement stmt = 
			getConnection().prepareStatement(select);
			
        stmt.setString(1, nome);
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            funcionario = new Funcionario();
            funcionario.setCpf(rs.getLong("cpf"));
            funcionario.setNome(rs.getString("nome"));
            funcionario.setDiaNasc(rs.getString("diaNasc"));
            funcionario.setMesNasc(rs.getString("mesNasc"));
            funcionario.setAnoNasc(rs.getString("anoNasc"));
        }

        rs.close();
        stmt.close();
        getConnection().close();

        return funcionario;
    }
}

