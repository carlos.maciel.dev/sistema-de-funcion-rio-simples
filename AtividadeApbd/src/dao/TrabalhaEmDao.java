/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Projeto;
import model.TrabalhaEm;

/**
 *
 * @author CARLO
 */
public class TrabalhaEmDao extends GenericDAO{
    public void salvar(TrabalhaEm trabalha) throws SQLException {
        String insert = "INSERT INTO Trabalha_em(cpf, codProjeto, horas) VALUES(?,?,?)";
        save(insert, trabalha.getCpf(), trabalha.getProjeto(), trabalha.getHoras());
    }

    public void alterar(TrabalhaEm trabalha) throws SQLException {
        String update = "UPDATE Trabalha_em " +
                "SET cpf = ?, codProjeto = ?, horas = ?" +
                "WHERE cpf = ?";
        update(update, trabalha.getCpf(), trabalha.getProjeto(), trabalha.getHoras());
    }

    public void excluir(long cpf) throws SQLException {
        String delete = "DELETE FROM Trabalha_em WHERE cpf = ?";
        delete(delete, cpf);
    }

    public List findTrabalhos() throws SQLException {
        List trabalhos = new ArrayList();

        String select = "SELECT * FROM Trabalha_em";

        PreparedStatement stmt = 
			getConnection().prepareStatement(select);
			
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            TrabalhaEm trabalha = new TrabalhaEm();
            trabalha.setCpf(rs.getInt("cpf"));
            trabalha.setProjeto(rs.getInt("codProjeto"));
            trabalha.setHoras(rs.getInt("horas"));
            

            
            trabalhos.add(trabalha);
        }

        rs.close();
        stmt.close();
        getConnection().close();

        return trabalhos;
    }

    public TrabalhaEm findByCodigo(int cpf) throws SQLException {
        String select = "SELECT * FROM Trabalha_em WHERE cpf = ?";
        TrabalhaEm trabalho = null;
        PreparedStatement stmt = 
			getConnection().prepareStatement(select);
			
        stmt.setInt(1, cpf);
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            TrabalhaEm trabalha = new TrabalhaEm();
            trabalha.setCpf(rs.getInt("cpf"));
            trabalha.setProjeto(rs.getInt("codProjeto"));
            trabalha.setHoras(rs.getInt("horas"));
        }

        rs.close();
        stmt.close();
        getConnection().close();

        return trabalho;
    }
}
