/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author CARLO
 */
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Departamento;
import model.Funcionario;

public class DepartamentoDao extends GenericDAO {

    public void salvar(Departamento departamento) throws SQLException {
        String insert = "INSERT INTO Departamento(codigo, nome) VALUES(?,?)";
        save(insert, departamento.getCodigo(), departamento.getNome());
    }

    public void alterar(Departamento departamento) throws SQLException {
        String update = "UPDATE Departamento " +
                "SET codigo = ?, nome = ?" +
                "WHERE codigo = ?";
        update(update, departamento.getCodigo(), departamento.getNome());
    }

    public void excluir(long codigo) throws SQLException {
        String delete = "DELETE FROM Departamento WHERE codigo = ?";
        delete(delete, codigo);
    }

    public List findDepartamentos() throws SQLException {
        List departamentos = new ArrayList();

        String select = "SELECT * FROM Departamento";

        PreparedStatement stmt = 
			getConnection().prepareStatement(select);
			
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            Departamento departamento = new Departamento();
            departamento.setCodigo(rs.getInt("codigo"));
            departamento.setNome(rs.getString("nome"));
            
            departamentos.add(departamento);
        }

        rs.close();
        stmt.close();
        getConnection().close();

        return departamentos;
    }
    
    

    public Departamento findByName(String nome) throws SQLException {
        String select = "SELECT * FROM Departamento WHERE nome = ?";
        Departamento departamento = null;
        PreparedStatement stmt = 
			getConnection().prepareStatement(select);
			
        stmt.setString(1, nome);
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            departamento = new Departamento();
            departamento.setCodigo(rs.getInt("codigo"));
            departamento.setNome(rs.getString("nome"));
        }

        rs.close();
        stmt.close();
        getConnection().close();

        return departamento;
    }
}

