/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.ProjetoDao;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import javax.swing.JOptionPane;
import model.Departamento;
import model.Projeto;

/**
 *
 * @author CARLO
 */
public class ProjetoController {
    
    public void salvar(int codigo, String nome, String ativo, int departamento) 
		throws SQLException, ParseException 
	{
        Projeto projeto = new Projeto();
        projeto.setCodigo(codigo);
        projeto.setNome(nome);
        projeto.setAtivo(ativo);
        projeto.setCodDepartamento(departamento);
        
       

        new ProjetoDao().salvar(projeto);
    }

    public void alterar(int codigo, String nome, String ativo, int departamento) 
		throws ParseException, SQLException 
	{
        
	Projeto projeto = new Projeto();
        projeto.setCodigo(codigo);
        projeto.setNome(nome);
        projeto.setAtivo(ativo);
        projeto.setCodDepartamento(departamento);
        
       

        new ProjetoDao().alterar(projeto);
    }

    public List listaProjetos() {
        ProjetoDao dao = new ProjetoDao();
        try {
            return dao.findProjetos();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, 
				"Problemas ao localizar projeto" + 
				e.getLocalizedMessage()
			);
        }
        return null;
    }

    public void excluir(int codigo) throws SQLException {
        new ProjetoDao().excluir(codigo);
    }

    public Projeto buscaProjetoPorNome(String nome) throws SQLException {
        ProjetoDao dao = new ProjetoDao();
        return dao.findByName(nome);
    }
    
}
