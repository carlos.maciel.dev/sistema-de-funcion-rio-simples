/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author CARLO
 */


import dao.FuncionarioDao;
import javax.swing.*;
import java.sql.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import model.Funcionario;

public class FuncionarioController {

   

    public void salvar(long cpf, String nome, String diaNasc, String mesNasc, String anoNasc) 
		throws SQLException, ParseException 
	{
        Funcionario funcionario = new Funcionario();
        funcionario.setCpf(cpf);
        funcionario.setNome(nome);
        funcionario.setDiaNasc(diaNasc);
        funcionario.setMesNasc(mesNasc);
        funcionario.setAnoNasc(anoNasc);
       

        new FuncionarioDao().salvar(funcionario);
    }

    public void alterar(long cpf, String nome, String diaNasc, String mesNasc, String anoNasc) 
		throws ParseException, SQLException 
	{
        
	Funcionario funcionario = new Funcionario();
        funcionario.setCpf(cpf);
        funcionario.setNome(nome);
        funcionario.setDiaNasc(diaNasc);
        funcionario.setMesNasc(mesNasc);
        funcionario.setAnoNasc(anoNasc);

         new FuncionarioDao().alterar(funcionario);
    }

    public List listaFuncionarios() {
        FuncionarioDao dao = new FuncionarioDao();
        try {
            return dao.findFuncionarios();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, 
				"Problemas ao localizar funcionario" + 
				e.getLocalizedMessage()
			);
        }
        return null;
    }

    public void excluir(long cpf) throws SQLException {
        new FuncionarioDao().excluir(cpf);
    }

    public Funcionario buscaFuncionarioPorNome(String nome) throws SQLException {
        FuncionarioDao dao = new FuncionarioDao();
        return dao.findByName(nome);
    }
}
