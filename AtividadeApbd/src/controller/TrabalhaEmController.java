/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.ProjetoDao;
import dao.TrabalhaEmDao;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import javax.swing.JOptionPane;
import model.Projeto;
import model.TrabalhaEm;

/**
 *
 * @author CARLO
 */
public class TrabalhaEmController {
       public void salvar(int codigo, int projeto, int horas) 
		throws SQLException, ParseException 
	{
        TrabalhaEm trabalha = new TrabalhaEm();
        trabalha.setCpf(codigo);
        trabalha.setProjeto(projeto);
        trabalha.setHoras(horas);
       
        
       

        new TrabalhaEmDao().salvar(trabalha);
    }

    public void alterar(int codigo, int projeto, int horas) 
		throws ParseException, SQLException 
	{
        
	TrabalhaEm trabalha = new TrabalhaEm();
        trabalha.setCpf(codigo);
        trabalha.setProjeto(projeto);
        trabalha.setHoras(horas);
       
        
       

        new TrabalhaEmDao().alterar(trabalha);
    }

    public List listaProjetos() {
        TrabalhaEmDao dao = new TrabalhaEmDao();
        try {
            return dao.findTrabalhos();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, 
				"Problemas ao localizar trabalhos" + 
				e.getLocalizedMessage()
			);
        }
        return null;
    }

    public void excluir(int codigo) throws SQLException {
        new TrabalhaEmDao().excluir(codigo);
    }

    public TrabalhaEm buscaTrabalhoPorCpf(int cpf) throws SQLException {
        TrabalhaEmDao dao = new TrabalhaEmDao();
        return dao.findByCodigo(cpf);
    }
}
