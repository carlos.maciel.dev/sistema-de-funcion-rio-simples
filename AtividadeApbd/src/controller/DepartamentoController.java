/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author CARLO
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author CARLO
 */


import dao.DepartamentoDao;
import dao.FuncionarioDao;
import javax.swing.*;
import java.sql.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import model.Departamento;
import model.Funcionario;

public class DepartamentoController {

   

    public void salvar(int codigo, String nome) 
		throws SQLException, ParseException 
	{
        Departamento departamento = new Departamento();
        departamento.setCodigo(codigo);
        departamento.setNome(nome);
        
       

        new DepartamentoDao().salvar(departamento);
    }

    public void alterar(int codigo, String nome) 
		throws ParseException, SQLException 
	{
        
	Departamento departamento = new Departamento();
        departamento.setCodigo(codigo);
        departamento.setNome(nome);

         new DepartamentoDao().alterar(departamento);
    }

    public List listaDepartamentos() {
        DepartamentoDao dao = new DepartamentoDao();
        try {
            return dao.findDepartamentos();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, 
				"Problemas ao localizar departamento" + 
				e.getLocalizedMessage()
			);
        }
        return null;
    }

    public void excluir(int codigo) throws SQLException {
        new DepartamentoDao().excluir(codigo);
    }

    public Departamento buscaDepartamentoPorNome(String nome) throws SQLException {
        DepartamentoDao dao = new DepartamentoDao();
        return dao.findByName(nome);
    }
}
