/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

/**
 *
 * @author CARLO
 */


import controller.FuncionarioController;
import dao.ConexaoMySql;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Funcionario;

public class FuncionarioFrame extends JFrame {
    private Connection con;

   

    protected Connection getConnection() {
        return con;
    }
    

    private JLabel lbNome, lbCpf, lbdiaNasc, lbMesNasc, lbAnoNasc;
    private JTextField txtNome, txtCpf, txtDiaNasc, txtMesNasc, txtAnoNasc, txtLocalizar;
    private JButton btnSalvar, btnAlterar, btnExcluir, btnClear, btnLocalizar, btnProcedure;
    private JButton btnPrimeiro, btnProximo, btnAnterior, btnUltimo;

    private List funcionarioList = new FuncionarioController().listaFuncionarios();
    private int registroAtual = 0;
    private Long key;

    public FuncionarioFrame() {
        
        
        super("funcionarios");
        this.con = ConexaoMySql.getConexaoMySql();
        Container tela = getContentPane();
        setLayout(null);
        
        lbCpf = new JLabel("CPF");
        lbNome = new JLabel("Nome");
        lbdiaNasc = new JLabel("Dia");
        lbMesNasc = new JLabel("Mes"); 
        lbAnoNasc = new JLabel("Ano");        
       
       
        lbCpf.setBounds( 10, 10, 240, 15);
        lbNome.setBounds(10, 60, 240, 15);
        lbdiaNasc.setBounds(10, 110, 240, 15);
        lbMesNasc.setBounds(10, 155, 240, 15);
        lbAnoNasc.setBounds(10, 205, 240, 15);
        
        lbCpf.setForeground(Color.BLACK);
        lbNome.setForeground(Color.BLACK);
        lbdiaNasc.setForeground(Color.BLACK);
        lbMesNasc.setForeground(Color.BLACK);
        lbAnoNasc.setForeground(Color.BLACK);
                
                
        lbCpf.setFont(new Font("Courier New", Font.BOLD, 14));
        lbNome.setFont(new Font("Courier New", Font.BOLD, 14));
        lbdiaNasc.setFont(new Font("Courier New", Font.BOLD, 14));
        lbMesNasc.setFont(new Font("Courier New", Font.BOLD, 14));
        lbAnoNasc.setFont(new Font("Courier New", Font.BOLD, 14));
        

        tela.add(lbNome);
        tela.add(lbCpf);
        tela.add(lbdiaNasc);
        tela.add(lbMesNasc);
        tela.add(lbAnoNasc);
     

        txtCpf = new JTextField();               
        txtNome = new JTextField();
        txtDiaNasc = new JTextField();
        txtMesNasc = new JTextField();
        txtAnoNasc = new JTextField();

        txtCpf.setBounds(10, 25, 265, 30);
        txtNome.setBounds(10, 75, 265, 30);
        txtDiaNasc.setBounds(10, 125, 265, 30);
        txtMesNasc.setBounds(10, 175, 265, 30);
        txtAnoNasc.setBounds(10, 225, 265, 30);
        
        
        tela.add(txtCpf);
        tela.add(txtNome);
        tela.add(txtDiaNasc);
        tela.add(txtMesNasc);
        tela.add(txtAnoNasc);



        

        btnSalvar = new JButton("Salvar");
        btnAlterar = new JButton("Alterar");
        btnExcluir = new JButton("Excluir");
        btnClear = new JButton("Limpar");
        btnProcedure = new JButton("Adicionar Funcionario");
        btnPrimeiro = new JButton("|<");
        btnAnterior = new JButton("<<");
        btnProximo = new JButton(">>");
        btnUltimo = new JButton(">|");

        btnSalvar.setBounds(280, 25, 80, 20);
        btnAlterar.setBounds(280, 65, 80, 20);
        btnExcluir.setBounds(280, 105, 80, 20);
        btnProcedure.setBounds(280, 145, 180, 20);

        tela.add(btnSalvar);
        tela.add(btnAlterar);
        tela.add(btnExcluir);
        tela.add(btnProcedure);

        btnPrimeiro.setBounds(10, 270, 50, 20);
        btnAnterior.setBounds(60, 270, 50, 20);
        btnClear.setBounds(110, 270, 75, 20);
        btnProximo.setBounds(185, 270, 50, 20);
        btnUltimo.setBounds(235, 270, 50, 20);

        tela.add(btnPrimeiro);
        tela.add(btnAnterior);
        tela.add(btnClear);
        tela.add(btnProximo);
        tela.add(btnUltimo);

        JLabel lbLocalizar = new JLabel("Localizar por nome");
        lbLocalizar.setBounds(10, 290, 220, 20);

        txtLocalizar = new JTextField();
        txtLocalizar.setBounds(10, 320, 220, 30);

        btnLocalizar = new JButton("Ir");
        btnLocalizar.setBounds(230, 320, 55, 20);

        tela.add(lbLocalizar);
        tela.add(txtLocalizar);
        tela.add(btnLocalizar);

        setSize(800, 650);
        setVisible(true);
        setLocationRelativeTo(null);
        
        
        btnProcedure.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    salvarProcedure();
                    JOptionPane.showMessageDialog(null, "Funcionario salvo com sucesso!");
                } catch (SQLException ex) {
                    Logger.getLogger(FuncionarioFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        btnSalvar.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        onClickSalvar();
                    }
                }
        );

//        btnAlterar.addActionListener(
//                new ActionListener() {
//                   public void actionPerformed(ActionEvent e) {
//                       onClickAlterar();                    }
//                }
//       );
//
//        btnExcluir.addActionListener(
//                new ActionListener() {
//                    public void actionPerformed(ActionEvent e) {
//                        onClickExcluir();
//                    }
//                }
//        );

        btnClear.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        clearFields();
                        registroAtual = 0;
                    }
                }
        );

        btnLocalizar.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        onClickLocalizar();
                    }
                }
        );

        btnPrimeiro.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        onClickPrimeiro();
                    }
                }
        );
        btnAnterior.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        onClickAnterior();
                    }
                }
        );

        btnProximo.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        onClickProximo();
                    }
                }
        );

        btnUltimo.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        onClickUltimo();
                    }
                }
        );
    }

    private void onClickUltimo() {
        registroAtual =funcionarioList.size() - 1;
        getValores(registroAtual);
    }

    private void onClickProximo() {
        if (registroAtual != funcionarioList.size() - 1) {
            getValores(++registroAtual);
        }
    }

    private void onClickAnterior() {
        if (registroAtual != 0) {
            getValores(--registroAtual);
        }
    }

    private void onClickPrimeiro() {
        registroAtual = 0;
        getValores(registroAtual);
    }

    private void getValores(int index) {
        if (index <= funcionarioList.size() - 1) {
            Funcionario funcionarioAtual = (Funcionario) funcionarioList.get(index);
            long x;
            x = funcionarioAtual.getCpf();
            String s = String.valueOf(x);
            txtCpf.setText(s);
            txtNome.setText(funcionarioAtual.getNome());
            txtDiaNasc.setText(funcionarioAtual.getDiaNasc());
            txtMesNasc.setText(funcionarioAtual.getMesNasc());
            txtAnoNasc.setText(funcionarioAtual.getAnoNasc());

            
           
        }
    }

//    private void onClickAlterar() {
//        FuncionarioController fc = new FuncionarioController();
//       
//		long id = 0L;
//		
//		if (key == null) {
//         id = funcionarioList.get(registroAtual).getId();
//       } else {
//          id = key;            key = null;
//        }
//		
//        try {
//            fc.alterar(Integer.parseInt(txtCpf.getText()), txtNome.getText(), txtDiaNasc.getText(), txtMesNasc.getText(), txtAnoNasc.getText());
//            JOptionPane.showMessageDialog(this, "Funcionario alterado com sucesso!");
//            clearFields();
//            funcionarioList = new FuncionarioController().listaFuncionarios();
//        } catch (SQLException e) {
//            JOptionPane.showMessageDialog(this, "Nao foi possivel alterar funcionario!n" + e.getLocalizedMessage());
//        } catch (ParseException e) {
//            JOptionPane.showMessageDialog(this, "Data possui formato inválido!n" + e.getLocalizedMessage());
//        }
//    }

    private void onClickSalvar() {
        FuncionarioController fc = new FuncionarioController();
        try {
            fc.salvar(Integer.parseInt(txtCpf.getText()), txtNome.getText(), txtDiaNasc.getText(), txtMesNasc.getText(), txtAnoNasc.getText());
            JOptionPane.showMessageDialog(this, "Funcionario salvo com sucesso!");
            clearFields();
            funcionarioList = new FuncionarioController().listaFuncionarios();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this, 
				"Nao foi possivel salvar contato!n" + 
				e.getLocalizedMessage()
			);
        } catch (ParseException e) {
            JOptionPane.showMessageDialog(this, 
				"Data possui formato inválido!n" + 
				e.getLocalizedMessage()
		);
        }
    }

//    private void onClickExcluir() {
//        FuncionarioController fc = new FuncionarioController();
//        ;
//        try {
//            fc.excluir(id);
//            JOptionPane.showMessageDialog(this, "Funcionario excluido com sucesso!");
//            clearFields();
//            funcionarioList = new FuncionarioController().listaFuncionarios();
//        } catch (SQLException e) {
//            JOptionPane.showMessageDialog(this, 
//				"Nao foi possivel excluir o funcionario!n" + 
//				e.getLocalizedMessage()
//			);
//        }
//    }

    private void onClickLocalizar() {
        FuncionarioController fc = new FuncionarioController();
        try {
            Funcionario f = fc.buscaFuncionarioPorNome(txtLocalizar.getText());
           
            txtNome.setText(f.getNome());
            
			key = f.getCpf();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this, 
				"Ocorreu um erro, tente novamente!n" + 
				e.getLocalizedMessage()
			);
        } catch (NullPointerException e){
            JOptionPane.showMessageDialog(this, 
				"Contato não localizdo ou não existe!n" + 
				e.getLocalizedMessage()
			);
        }
    }

    private void clearFields() {
        txtNome.setText("");
        txtCpf.setText("");
        txtDiaNasc.setText("");
        txtMesNasc.setText("");
        txtAnoNasc.setText("");

        txtLocalizar.setText("");
    }
    private void salvarProcedure() throws SQLException{
        
        Funcionario funcionario = new Funcionario();
        funcionario.setCpf(Long.parseLong(txtCpf.getText()));
        funcionario.setNome(txtNome.getText());
        funcionario.setDiaNasc(txtDiaNasc.getText());
        funcionario.setMesNasc(txtMesNasc.getText());
        funcionario.setAnoNasc(txtAnoNasc.getText());
        
        CallableStatement callStmt = con.prepareCall("call sp_inserirFuncionario(?,?,?,?,?)"); // setando parâmetros
        callStmt.setString("cpf",txtCpf.getText()); 
        callStmt.setString("nome", txtNome.getText());
        callStmt.setString("diaNasc", txtDiaNasc.getText());
        callStmt.setString("mesNasc", txtMesNasc.getText());
        callStmt.setString("anoNasc", txtAnoNasc.getText());
        callStmt.execute();
        
    }

    public static void main(String[] args) {
        FuncionarioFrame frame = new FuncionarioFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
