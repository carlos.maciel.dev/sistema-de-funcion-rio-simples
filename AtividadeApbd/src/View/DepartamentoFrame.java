/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

/**
 *
 * @author CARLO
 */
import controller.DepartamentoController;
import controller.FuncionarioController;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import model.Departamento;
import model.Funcionario;

public class DepartamentoFrame extends JFrame {

    private JLabel lbNome, lbCodigo,lbLocalizar;
    private JTextField txtNome, txtCodigo, txtLocalizar;
    private JButton btnSalvar, btnAlterar, btnExcluir, btnClear, btnLocalizar;
    private JButton btnPrimeiro, btnProximo, btnAnterior, btnUltimo;

    private List departamentoList = new DepartamentoController().listaDepartamentos();
    private int registroAtual = 0;
    private int key;

    public DepartamentoFrame() {
        super("departamentos");
        Container tela = getContentPane();
        setLayout(null);
        
        lbCodigo = new JLabel("Código");
        lbNome = new JLabel("Nome");
            
       
       
        lbCodigo.setBounds( 10, 10, 240, 15);
        lbNome.setBounds(10, 50, 240, 15);
       
        lbCodigo.setForeground(Color.BLACK);
        lbNome.setForeground(Color.BLACK);
       
                
                
        lbCodigo.setFont(new Font("Courier New", Font.BOLD, 14));
        lbNome.setFont(new Font("Courier New", Font.BOLD, 14));
       
        

        tela.add(lbNome);
        tela.add(lbCodigo);
        
     

        txtCodigo = new JTextField();               
        txtNome = new JTextField();
       

        txtCodigo.setBounds(10, 25, 265, 30);
        txtNome.setBounds(10, 65, 265, 30);
        
        
        
        tela.add(txtCodigo);
        tela.add(txtNome);
       



        

        btnSalvar = new JButton("Salvar");
        btnAlterar = new JButton("Alterar");
        btnExcluir = new JButton("Excluir");
        btnClear = new JButton("Limpar");
        btnPrimeiro = new JButton("|<");
        btnAnterior = new JButton("<<");
        btnProximo = new JButton(">>");
        btnUltimo = new JButton(">|");

        btnSalvar.setBounds(280, 25, 80, 20);
        btnAlterar.setBounds(280, 65, 80, 20);
        btnExcluir.setBounds(280, 105, 80, 20);

        tela.add(btnSalvar);
        tela.add(btnAlterar);
        tela.add(btnExcluir);

        btnPrimeiro.setBounds(10, 130, 50, 20);
        btnAnterior.setBounds(60, 130, 50, 20);
        btnClear.setBounds(110, 130, 75, 20);
        btnProximo.setBounds(185, 130, 50, 20);
        btnUltimo.setBounds(235, 130, 50, 20);

        tela.add(btnPrimeiro);
        tela.add(btnAnterior);
        tela.add(btnClear);
        tela.add(btnProximo);
        tela.add(btnUltimo);

        JLabel lbLocalizar = new JLabel("Localizar por nome");
        lbLocalizar.setBounds(10, 255, 220, 20);

        txtLocalizar = new JTextField();
        txtLocalizar.setBounds(10, 280, 220, 20);

        btnLocalizar = new JButton("Ir");
        btnLocalizar.setBounds(230, 280, 55, 20);

        tela.add(lbLocalizar);
        tela.add(txtLocalizar);
        tela.add(btnLocalizar);

        setSize(600, 400);
        setVisible(true);
        setLocationRelativeTo(null);

        btnSalvar.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        onClickSalvar();
                    }
                }
        );

//        btnAlterar.addActionListener(
//                new ActionListener() {
//                   public void actionPerformed(ActionEvent e) {
//                       onClickAlterar();                    }
//                }
//       );
//
//        btnExcluir.addActionListener(
//                new ActionListener() {
//                    public void actionPerformed(ActionEvent e) {
//                        onClickExcluir();
//                    }
//                }
//        );

        btnClear.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        clearFields();
                        registroAtual = 0;
                    }
                }
        );

        btnLocalizar.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        onClickLocalizar();
                    }
                }
        );

        btnPrimeiro.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        onClickPrimeiro();
                    }
                }
        );
        btnAnterior.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        onClickAnterior();
                    }
                }
        );

        btnProximo.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        onClickProximo();
                    }
                }
        );

        btnUltimo.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        onClickUltimo();
                    }
                }
        );
    }

    private void onClickUltimo() {
        registroAtual = departamentoList.size() - 1;
        getValores(registroAtual);
    }

    private void onClickProximo() {
        if (registroAtual != departamentoList.size() - 1) {
            getValores(++registroAtual);
        }
    }

    private void onClickAnterior() {
        if (registroAtual != 0) {
            getValores(--registroAtual);
        }
    }

    private void onClickPrimeiro() {
        registroAtual = 0;
        getValores(registroAtual);
    }

    private void getValores(int index) {
        if (index <= departamentoList.size() - 1) {
            Departamento departamentoAtual = (Departamento) departamentoList.get(index);
            long x;
            x = departamentoAtual.getCodigo();
            String s = String.valueOf(x);
            txtCodigo.setText(s);
            txtNome.setText(departamentoAtual.getNome());
            

            
           
        }
    }

//    private void onClickAlterar() {
//        DepartamentoController dc = new DepartamentoController();
//       
//		long id = 0L;
//		
//		if (key == null) {
//           id = departamentoList.get(registroAtual).getId();
//        } else {
//          id = key;
//            key = null;
//        }
//		
//        try {
//            dc.alterar(Integer.parseInt(txtCodigo.getText()), txtNome.getText());
//            JOptionPane.showMessageDialog(this, "Funcionario alterado com sucesso!");
//            clearFields();
//            departamentoList = new DepartamentoController().listaDepartamentos();
//        } catch (SQLException e) {
//            JOptionPane.showMessageDialog(this, "Nao foi possivel alterar departamento!n" + e.getLocalizedMessage());
//        } catch (ParseException e) {
//            JOptionPane.showMessageDialog(this, "Data possui formato inválido!n" + e.getLocalizedMessage());
//        }
//    }

    private void onClickSalvar() {
        DepartamentoController dc = new DepartamentoController();
        try {
            dc.salvar(Integer.parseInt(txtCodigo.getText()), txtNome.getText());
            JOptionPane.showMessageDialog(this, "Departamento salvo com sucesso!");
            clearFields();
            departamentoList = new FuncionarioController().listaFuncionarios();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this, 
				"Nao foi possivel salvar departamento!n" + 
				e.getLocalizedMessage()
			);
        } catch (ParseException e) {
            JOptionPane.showMessageDialog(this, 
				"Data possui formato inválido!n" + 
				e.getLocalizedMessage()
		);
        }
    }

//    private void onClickExcluir() {
//        FuncionarioController fc = new FuncionarioController();
//        long id = funcionarioList.get(registroAtual).getId();
//        try {
//            fc.excluir(id);
//            JOptionPane.showMessageDialog(this, "Funcionario excluido com sucesso!");
//            clearFields();
//            funcionarioList = new FuncionarioController().listaFuncionarios();
//        } catch (SQLException e) {
//            JOptionPane.showMessageDialog(this, 
//				"Nao foi possivel excluir o funcionario!n" + 
//				e.getLocalizedMessage()
//			);
//        }
//    }

    private void onClickLocalizar() {
        DepartamentoController dc = new DepartamentoController();
        try {
            Departamento d = dc.buscaDepartamentoPorNome(txtLocalizar.getText());
           
            txtNome.setText(d.getNome());
            
			key = d.getCodigo();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this, 
				"Ocorreu um erro, tente novamente!n" + 
				e.getLocalizedMessage()
			);
        } catch (NullPointerException e){
            JOptionPane.showMessageDialog(this, 
				"Departamento não localizdo ou não existe!n" + 
				e.getLocalizedMessage()
			);
        }
    }

    private void clearFields() {
        txtNome.setText("");
        txtCodigo.setText("");
       

        txtLocalizar.setText("");
    }

    
    
    public static void main(String[] args) {
        DepartamentoFrame frame = new DepartamentoFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
