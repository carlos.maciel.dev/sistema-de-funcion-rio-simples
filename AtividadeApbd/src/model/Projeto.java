/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author CARLO
 */
public class Projeto {
    
    public int codigo;
    public String nome;
    public String ativo;
    public int codDepartamento;

    public Projeto() {
    }

    public Projeto(int codigo, String nome, String ativo, int codDepartamento) {
        this.codigo = codigo;
        this.nome = nome;
        this.ativo = ativo;
        this.codDepartamento = codDepartamento;
    }

   

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getAtivo() {
        return ativo;
    }

    public void setAtivo(String ativo) {
        this.ativo = ativo;
    }

    public int getCodDepartamento() {
        return codDepartamento;
    }

    public void setCodDepartamento(int codDepartamento) {
        this.codDepartamento = codDepartamento;
    }

    
    
    

    
}
