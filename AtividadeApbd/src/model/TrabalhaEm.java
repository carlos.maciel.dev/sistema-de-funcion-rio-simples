/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author CARLO
 */
public class TrabalhaEm {
    
    public long cpf;
    public int projeto;
    public int horas;
   

    public TrabalhaEm() {
    }

    public TrabalhaEm(long cpf, int projeto, int horas) {
        this.cpf = cpf;
        this.projeto = projeto;
        this.horas = horas;
    }

    public long getCpf() {
        return cpf;
    }

    public void setCpf(long cpf) {
        this.cpf = cpf;
    }

    public int getProjeto() {
        return projeto;
    }

    public void setProjeto(int projeto) {
        this.projeto = projeto;
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }

    
    

    
}
